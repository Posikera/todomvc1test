package net.poskera.jv;

import org.junit.Test;
import org.openqa.selenium.Keys;
import static net.poskera.jv.seperate.TodoMVC.*;
import static com.codeborne.selenide.Condition.visible;

public class TodoMVCOperationTest extends AtTodoMVCPageAfterClearDataAfterEachTest {

    @Test
    public void testAllFiler() {
        //add new tasks
        addTask("1");
        addTask("2");
        addTask("3");
        addTask("4");
        assertTasksAre("1", "2", "3", "4");

        //edit task and break
        startEditingTask("3");
        editingTask.setValue("try editing and break").sendKeys(Keys.ESCAPE);
        assertTasksAre("1", "2", "3", "4");

        //edit task and Delete it
        editTask("4", "4 edited");
        deleteTask("4 edited");
        assertItemsLeftCounter(3);
        assertTasksAre("1", "2", "3");

        //mark one task and clear completed
        toggleTask("3");
        clearCompleted();
        assertTasksAre("1", "2");
        assertItemsLeftCounter(2);

        //reopen task
        toggleTask("2");
        assertItemsLeftCounter(1);
        filterCompleted();
        assertVisibleTasksAre("2");
        filterAll();
        toggleTask("2");
        assertItemsLeftCounter(2);
        buttonClearCompleted.shouldNotBe(visible);

        //Mark all tasks as completed and clear
        toggleAll();
        assertItemsLeftCounter(0);
        clearCompleted();
        tasks.shouldHaveSize(0);
    }

    @Test
    public void testActiveFiler() {
        //Given filtered tasks at active filter
        addTask("1");
        addTask("2");
        toggleTask("2");
        toggleTask("2");
        toggleTask("1");
        filterActive();
        assertItemsLeftCounter(1);
        assertVisibleTasksAre("2");

        //addTask new tasks, go to all filter and back
        addTask("3");
        addTask("4");
        assertVisibleTasksAre("2", "3", "4");
        assertItemsLeftCounter(3);
        filterAll();
        assertVisibleTasksAre("1","2", "3", "4");
        assertItemsLeftCounter(3);
        filterActive();

        //editTask task name and Delete this task
        editTask("4", "4 edited");
        deleteTask("4 edited");
        assertItemsLeftCounter(2);
        assertVisibleTasksAre("2", "3");

        //mark task as completed and filtering
        toggleTask("3");
        assertItemsLeftCounter(1);
        assertVisibleTasksAre("2");
        filterCompleted();
        assertVisibleTasksAre("1", "3");
        filterActive();

        //Mark all tasks as completed
        toggleAll();
        assertItemsLeftCounter(0);
        visibleTasks.shouldHaveSize(0);

        //reopen all tasks
        toggleAll();
        assertItemsLeftCounter(3);
        assertVisibleTasksAre("1", "2", "3");
    }

    @Test
    public void testCompletedFiler() {
        //Given filtered tasks at completed filter
        addTask("1");
        addTask("2");
        addTask("3");
        addTask("4");
        addTask("5");
        toggleTask("5");
        toggleTask("4");
        toggleTask("3");
        filterActive();
        toggleTask("2");
        filterCompleted();
        assertItemsLeftCounter(1);
        assertVisibleTasksAre("2", "3", "4", "5");

        //Delete task
        deleteTask("5");
        assertItemsLeftCounter(1);
        assertVisibleTasksAre("2", "3", "4");

        //Delete by editing to ""
        editTask("4","");
        assertItemsLeftCounter(1);
        assertVisibleTasksAre("2","3");



        //reopen task, go to active filter and back
        toggleTask("3");
        assertItemsLeftCounter(2);
        assertVisibleTasksAre("2");
        filterActive();
        assertVisibleTasksAre("1", "3");
        filterCompleted();

        //clear completed
        clearCompleted();
        visibleTasks.shouldHaveSize(0);
        assertItemsLeftCounter(2);
    }
}
