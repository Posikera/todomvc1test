package net.poskera.jv.seperate;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;


public class TodoMVC {

    public static ElementsCollection visibleTasks = $$("#todo-list>li").filter(visible);
    public static ElementsCollection tasks = $$("#todo-list>li");
    public static SelenideElement editingTask = tasks.findBy(cssClass("editing")).$(".edit");
    public static SelenideElement buttonClearCompleted = $("#clear-completed");

    @Step
    public static void toggleAll() {
        $("#toggle-all").click();
    }

    @Step
    public static void filterAll() {
        $("a[href='#/']").click();
    }

    @Step
    public static void filterActive() {
        $("a[href*='active']").click();
    }

    @Step
    public static void filterCompleted() {
        $("a[href*='completed']").click();
    }

    @Step
    public static void assertItemsLeftCounter(int itemsNumber) {
        $("#todo-count>strong").shouldHave(exactText(Integer.toString(itemsNumber)));
    }

    @Step
    public static void clearCompleted() {
        buttonClearCompleted.click();
        buttonClearCompleted.shouldBe(hidden);
    }

    @Step
    public static void addTask(String text) {
        $("#new-todo").setValue(text).pressEnter();
    }

    @Step
    public static void deleteTask(String taskName) {
        tasks.find(text(taskName)).hover().$(".destroy").click();
    }

    @Step
    public static void toggleTask(String text) {
        tasks.find(text(text)).$(".toggle").click();
    }

    @Step
    public static void editTask(String oldText, String newText) {
        startEditingTask(oldText);
        editingTask.setValue(newText).pressEnter();
    }

    @Step
    public static void startEditingTask(String text) {
        tasks.find(text(text)).find("label").doubleClick();
    }

    @Step
    public static void assertVisibleTasksAre(String... texts) {
        visibleTasks.shouldHave(exactTexts(texts));
    }

    @Step
    public static void assertTasksAre(String... texts) {
        tasks.shouldHave(exactTexts(texts));
    }


}