package net.poskera.jv;

import com.codeborne.selenide.Screenshots;
import com.google.common.io.Files;
import org.junit.After;
import org.junit.Before;
import ru.yandex.qatools.allure.annotations.Attachment;

import java.io.File;
import java.io.IOException;

import static com.codeborne.selenide.Selenide.executeJavaScript;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.screenshot;

/**
 * Created by Home on 08.08.2015.
 */
public class AtTodoMVCPageAfterClearDataAfterEachTest {
    @Before
    public void ClearData() {
        open("http://todomvc.com/examples/troopjs_require/");
    }

    @After
    public void tearDown() throws IOException {
        screenshot();
        executeJavaScript("localStorage.clear()");
    }

    @Attachment(type = "image/png")
    public byte[] screenshot() throws IOException {
        File screenshot = Screenshots.getScreenShotAsFile();
        return Files.toByteArray(screenshot);
    }
}
